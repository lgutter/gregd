# 1. Intro

The purpose of this protocol is to define set boundaries and rules of how to
communicate with the gregd daemon.

# 2. Establishing Connection

Upon launching gregd, a unix socket file is created. The location is determined
by the following algorithm:

 1. If environment variable `XDG_RUNTIME_DIR` is defined, the location is
    `$XDG_RUNTIME_DIR/gregd.sock`.
 2. Otherwise, if the environment variable `USER` is defined, the location is
    `/tmp/gregd-$USER.sock`.
 3. If neither environment variable is defined, the location is
    `/tmp/gregd.sock`.

The client SHOULD implement the same algorithm.

# 3. Message Format

A request or response message is defined as a JSON object followed by a
NULL-byte. The JSON object MUST consist of a top-level object with the key
`type`. The `type` key represents the type of the message, and MUST be one
of the types defined in "Message Types".

# 4. Message Types

A message can either be a request or a response message. Below are the
currently implemented request and response message types.

## 4.1. Request Messages

### 4.1.1. ping

Test if the connection is working.

_no fields are associated with this request type._

| Possible response types | Description                       |
|-------------------------|-----------------------------------|
| `ok`                    | Confirmation of the ping request. |

### 4.1.2. log_message

Send a message to be logged by gregd.

| Fields    | Type     | Required | Description               |
|-----------|----------|----------|---------------------------|
| `message` | `string` | Yes      | The message to be logged. |

| Possible response types | Description                       |
|-------------------------|-----------------------------------|
| `ok`                    | The request has been accepted.    |

### 4.1.3. reload

Reloads the configuration files.

_no fields are associated with this request type._

| Possible response types | Description                                      |
|-------------------------|--------------------------------------------------|
| `ok`                    | The request has been accepted.                   |
| `config_error`          | Error when reading/parsing config.               |
| `shutting_down`         | Unable to process request, we are shutting down. |

### 4.1.4. start

Start a service as specified in the config. If we previously failed to start
a required process, try again.

| Fields    | Type     | Required | Description                       |
|-----------|----------|----------|-----------------------------------|
| `service` | `string` | Yes      | The name of the service to start. |

| Possible response types | Description                                       |
|-------------------------|---------------------------------------------------|
| `ok`                    | The request has been accepted.                    |
| `unknown_service`       | No service exists with the provided service name. |
| `shutting_down`         | Unable to process request, we are shutting down.  |

### 4.1.5. stop

Stop a service, killing all processes of that service. If autostart is set to
true the service will be started again when reloading the config.

| Fields    | Type     | Required | Description                       |
|-----------|----------|----------|-----------------------------------|
| `service` | `string` | Yes      | The name of the service to stop.  |

| Possible response types | Description                                       |
|-------------------------|---------------------------------------------------|
| `ok`                    | The request has been accepted.                    |
| `unknown_service`       | No service exists with the provided service name. |
| `shutting_down`         | Unable to process request, we are shutting down.  |

### 4.1.6. restart

Restart a service. This is functionally equivalent of sending the stop request
and sending the start request in sequence. Note that the processes are started
immediately and do not wait for the old processes to die.

| Fields    | Type     | Required | Description                         |
|-----------|----------|----------|-------------------------------------|
| `service` | `string` | Yes      | The name of the service to restart. |

| Possible response types | Description                                       |
|-------------------------|---------------------------------------------------|
| `ok`                    | The request has been accepted.                    |
| `unknown_service`       | No service exists with the provided service name. |
| `shutting_down`         | Unable to process request, we are shutting down.  |

### 4.1.7. status

Request the status of a the given service. If no service has been given, request
the status of all services.

| Fields    | Type     | Required | Description                         |
|-----------|----------|----------|-------------------------------------|
| `service` | `string` | No       | The name of the service.            |

| Possible response types | Description                                       |
|-------------------------|---------------------------------------------------|
| `status`                | The request has been accepted.                    |
| `unknown_service`       | No service exists with the provided service name. |
| `shutting_down`         | Unable to process request, we are shutting down.  |

### 4.1.8. shutdown

Request the daemon to shutdown gracefully. Equivalent of sending a `SIGINT` or
`SIGTERM` to the daemon. Sending a shutdown requests when we are already
shutting down is a no-op.

_no fields are associated with this request type._

| Possible response types | Description                                       |
|-------------------------|---------------------------------------------------|
| `ok`                    | The request has been accepted.                    |

## 4.2. Response Types

### 4.2.1. ok

Generic indication of success.

_no fields are associated with this response type._

### 4.2.2. bad_request

The daemon MAY send this in case it received data from the client which is not
compliant with the protocol.

_no fields are associated with this response type._

### 4.2.3. config_error

Indicates that something went wrong when trying apply the new config.

| Fields    | Type     | Description                           |
|-----------|----------|---------------------------------------|
| `message` | `string` | A message describing what went wrong. |

### 4.2.4. unknown_service

Indicates that the provided service in the request does not exist in the
presently loaded config.

| Fields    | Type     | Description                           |
|-----------|----------|---------------------------------------|
| `service` | `string` | The name of the non-existant service. |

### 4.2.5. status

The status of any amount of services.

| Fields     | Type                    | Description                         |
|------------|-------------------------|-------------------------------------|
| `services` | `array` of service info | Info about every requested service. |

#### 4.2.5.1. service info

| Fields      | Type                    | Description                         |
|-------------|-------------------------|-------------------------------------|
| `name`      | `string`                | The name of the service.            |
| `status`    | service status          | The status of the service.          |
| `processes` | `array` of process info | The processes part of this service. |

#### 4.2.5.2. service status

Service status is a `string` with one of the following values:

| Name       | Description                                             |
|------------|---------------------------------------------------------|
| `active`   | The service is started and all processes are alive.     |
| `inactive` | The service is not started.                             |
| `degraded` | The service is started but not all processes are alive. |

#### 4.2.5.3. process info

| Fields   | Type           | Description                |
|----------|----------------|----------------------------|
| `pid`    | `number`       | The pid of this process.   |
| `status` | process status | The status of the process. |

#### 4.2.5.4. process status

Process status is a `string` with one of the following values:

| Name          | Description                                          |
|---------------|------------------------------------------------------|
| `starting`    | Not terminating, alive for less than `starttime_ms`. |
| `running`     | Not terminating, alive at least `startime_ms`.       |
| `terminating` | We have send a kill signal to the process.           |

### 4.2.6. shutting_down

Indicates that the request could not be completed because the daemon is shutting
down.

_no fields are associated with this response type._

# 5. Message Flow

The messages send by the client MUST be of the request type. The messages send
by the daemon MUST be of the response type. The client MAY send a request at
any time. After receiving and processing a request, the daemon MUST respond with
a message that corresponds to the "Possible response types" of a request. The
daemon MUST NOT send a response at any other time.
