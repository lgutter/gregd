# 1. Intro
This project is a rust implementation of the 42 project `Taskmaster`. Its name
is a reference to the one true taskmaster, Greg Davies.


# 2. File locations
Gregd uses a few different files during operation, so it's good to know what
will be where, and how you can customize what goes where.

## 2.1. configuration file
The configuration file for gregd is called `services.yaml`, and it's location is
determined as follows:  
- If the environment variable `XDG_CONFIG_HOME` is set, the config file is
  assumed to be at `${XDG_CONFIG_HOME}/gregd/services.yaml`.
- Otherwise, if the environment variable `HOME` is set, the config file is
  assumed to be at `${HOME}/gregd/services.yaml`.
- Otherwise, the config file is assumed to be in the current directory. (i.e.
  `./services.yaml`)

## 2.2. unix socket file
- If the environment variable `XDG_RUNTIME_DIR` is set, the socket file will be
 `${XDG_RUNTIME_DIR}/gregd.sock`.
- Otherwise, if the environment variable `USER` is set, the socket file will be
 `/tmp/gregd-${USER}.sock`.
- Otherwise, the socket file will be `/tmp/gregd.sock`.

## 2.3. log file
- If the environment variable `XDG_STATE_HOME` is set, the log file will be
  `${XDG_STATE_HOME}/gregd/gregd.log`.
- Otherwise, if the environment variable `HOME` is set, the socket file will be
  `${HOME}/.local/state/gregd/gregd.log`.
- Otherwise, the log file will be in the current directory. (i.e. `./gregd.log`)

# 3. Required features
As per the project subject, we support the following required features:
- The program needs to manage services defined in a config file by starting,
  stopping, and restarting processes as needed based on the configuration.
- The user must be able to use the following commands:
  - status: See the status of all the services described in the config file
  - start: start the given service
  - stop: stop the given service
  - restart: restart the given service
  - reload: Reload the configuration file without stopping the main program
  - stop: Stop the main program (gregd)
- The configuration file must allow the user to specify the following, for each
  service that is to be supervised:
  - The command to use to launch the service
  - The number of processes to start and keep running
  - Whether to start this service at launch or not
  - Whether the service should be restarted always, never, or on unexpected
    exits only
  - Which return codes represent an "expected" exit status
  - How long the service should be running after it’s started for it to be
    considered "successfully started"
  - How many times a restart should be attempted before aborting
  - Which signal should be used to stop (i.e. exit gracefully) the service
  - How long to wait after a graceful stop before killing the service
  - Options to discard the service’s stdout/stderr or to redirect them to files
  - Environment variables to set before launching the service
  - A working directory to set before launching the service
  - A umask to set before launching the service
- SIGHUP must reload the configuration

# 4. Bonus features
Besides these required features, we have implemented the following bonus
features:
- gregd is a daemon that runs on its own, and can be controlled externally.
- Communication with gregd follows a strictly defined protocol, so anyone could
  build a control utility for it, in any language they wish. (see `PROTOCOL.md`)
- This project also contains `libgreg`, a library meant to facilitate easy
  implementation of the protocol and communication in rust.
- Extended status command, capable of giving the status for ALL services, or
  just the provided service, including identification of degraded services.
- Improved logging, logging to `stderr` besides the log file, and with support
  for the following log levels (configurable through the environment variable
  `GREGD_LOG_LEVEL`):
  - debug
  - info
  - warning
  - error
  - fatal
