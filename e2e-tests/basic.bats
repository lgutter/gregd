#!/bin/bash

setup() {
    EXECUTABLE_DIR="target/debug/"
    TEST_OUTPUT_DIR="$(mktemp -d)"
    export XDG_STATE_HOME=${TEST_OUTPUT_DIR}/state
    export XDG_RUNTIME_DIR=${TEST_OUTPUT_DIR}/runtime
    export XDG_CONFIG_HOME=${TEST_OUTPUT_DIR}/config
    export TIMESTAMP_REGEX="\[[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\]"
    mkdir -p ${XDG_RUNTIME_DIR}
    mkdir -p ${XDG_CONFIG_HOME}/gregd
    cp e2e-tests/empty-config.yaml ${XDG_CONFIG_HOME}/gregd/services.yaml
    export PATH="${PATH}:${EXECUTABLE_DIR}"

    echo "----------"
    echo "Use the following exports to create test environment for debugging:"
    echo "export TEST_OUTPUT_DIR=${TEST_OUTPUT_DIR}"
    echo "export XDG_STATE_HOME=${XDG_STATE_HOME}"
    echo "export XDG_RUNTIME_DIR=${XDG_RUNTIME_DIR}"
    echo "export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}"
    echo "----------"
}

success_teardown() {
    rm -rf ${TEST_OUTPUT_DIR}
}

function teardown() {
    pkill -15 gregd || true
}

catch_output() {
    cmd=${1}
    shift
    ${cmd} "$@" > "${TEST_OUTPUT_DIR}/${cmd}_stdout.log" 2>"${TEST_OUTPUT_DIR}/${cmd}_stderr.log" || true
}

is_empty_file() {
    [ -f ${1} ] && [ ! -s ${1} ]
}

# grep for given regex expression in given file.
# prints actual content on failure.
# usage:
# regex_file_check <regular expression> <file>
regex_file_check() {
    if ! grep -qE "${1}" "${2}"; then
        echo "----------"
        echo "Regex check failed!"
        echo "Regex: \"${1}\""
        echo "File contents with cat -e:"
        cat -e "${2}"
        echo "----------"
        return 1
    fi
    return 0
}

# send given string formatted by printf to gregd socket.
# timeouts after 1 second to make sure we are sending a response in a reasonable time.
# usage:
# nc_send_to_socket <string_to_send> 
nc_send_to_socket() {
    printf "${1}" | nc -Uw1 ${XDG_RUNTIME_DIR}/gregd.sock > ${TEST_OUTPUT_DIR}/nc_stdout.log
}

@test "send log_message to gregd" {
    catch_output gregd &
    sleep .01

    catch_output gregctl log "hello world"
    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Message from client: hello world.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Message from client: hello world$" ${XDG_STATE_HOME}/gregd/gregd.log


    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stderr.log
    success_teardown
}

@test "send status to gregd for non-existent service" {
    catch_output gregd &
    sleep .01

    catch_output gregctl status foobar
    pkill -2 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log

    regex_file_check "^gregctl: Unknown service foobar, maybe you need to reload the config\?$" ${TEST_OUTPUT_DIR}/gregctl_stderr.log

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    success_teardown
}

@test "send status to gregd for existing service" {
    cp e2e-tests/test-config.yaml ${XDG_CONFIG_HOME}/gregd/services.yaml
    catch_output gregd &
    sleep .01

    catch_output gregctl status sleepone
    pkill -2 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Got status for service sleepone.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Got status for service sleepone$" ${XDG_STATE_HOME}/gregd/gregd.log

    regex_file_check "^sleepone \(active\)$" ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    regex_file_check "^    [0-9]+ +running$" ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stderr.log

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "send ping to gregd without gregctl" {
    export GREGD_LOG_LEVEL=debug
    catch_output gregd &
    sleep .01

    nc_send_to_socket '{"type":"ping"}\0'

    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Received ping request.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd debug: Received ping request$" ${XDG_STATE_HOME}/gregd/gregd.log

    cat -e ${TEST_OUTPUT_DIR}/nc_stdout.log | grep -q '{"type":"ok"}^@'

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "send ping to gregd using gregctl" {
    export GREGD_LOG_LEVEL=debug
    catch_output gregd &
    sleep .01

    catch_output gregctl ping

    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Received ping request.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd debug: Received ping request$" ${XDG_STATE_HOME}/gregd/gregd.log

    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stderr.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "send request of invalid type to gregd without gregctl" {
    catch_output gregd &
    sleep .01

    nc_send_to_socket '{"type":"foo"}\0'

    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Unable to parse request: Invalid JSON: unknown variant \`foo\`, expected .* at line 1 column 13.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd warn: Unable to parse request: Invalid JSON: unknown variant \`foo\`, expected .* at line 1 column 13$" ${XDG_STATE_HOME}/gregd/gregd.log


    cat -e ${TEST_OUTPUT_DIR}/nc_stdout.log | grep -q '{"type":"bad_request"}^@'

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "send non-json request to gregd without gregctl" {
    catch_output gregd &
    sleep .01

    nc_send_to_socket 'hello world\0'

    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Unable to parse request: Invalid JSON: expected value at line 1 column 1.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd warn: Unable to parse request: Invalid JSON: expected value at line 1 column 1$" ${XDG_STATE_HOME}/gregd/gregd.log


    cat -e ${TEST_OUTPUT_DIR}/nc_stdout.log | grep -q '{"type":"bad_request"}^@'

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "error when running gregctl without a running gregd" {
    catch_output gregctl ping

    regex_file_check "^gregctl: Failed to send request: Failed to connect to ${XDG_RUNTIME_DIR}/gregd.sock: No such file or directory \(os error 2\)$" ${TEST_OUTPUT_DIR}/gregctl_stderr.log

    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    success_teardown
}

@test "invalid command for gregctl" {
    catch_output gregd &
    sleep .01

    catch_output gregctl foobar

    pkill -15 gregd

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*: Initiating shutdown.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*: No child processes currently alive, shutting down.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^ *3$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Initiating shutdown$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: No child processes currently alive, shutting down$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^ *3$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)

    regex_file_check "^gregctl: Invalid command: foobar$" ${TEST_OUTPUT_DIR}/gregctl_stderr.log

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    is_empty_file ${TEST_OUTPUT_DIR}/gregctl_stdout.log
    success_teardown
}

@test "start gregd without config file" {
    export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}/noconfig
    mkdir -p ${XDG_CONFIG_HOME}/gregd

    catch_output gregd &
    sleep .01

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Could not load config: Could not open file \"${XDG_CONFIG_HOME}/gregd/services.yaml\": No such file or directory \(os error 2\).*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^ *1$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)


    regex_file_check "^${TIMESTAMP_REGEX} gregd fatal: Could not load config: Could not open file \"${XDG_CONFIG_HOME}/gregd/services.yaml\": No such file or directory \(os error 2\)$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^ *1$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)


    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "start gregd with invalid config file" {
    export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}/invalid
    mkdir -p ${XDG_CONFIG_HOME}/gregd
    cp e2e-tests/test-config.yaml ${XDG_CONFIG_HOME}/gregd/services.yaml
    echo "invalid yaml line" >> ${XDG_CONFIG_HOME}/gregd/services.yaml
    catch_output gregd &
    sleep .01

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.*Could not load config: Parsing error: while parsing a block mapping, did not find expected key at line.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^ *1$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)


    regex_file_check "^${TIMESTAMP_REGEX} gregd fatal: Could not load config: Parsing error: while parsing a block mapping, did not find expected key at line [0-9]+ column [0-9]+$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^ *1$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)


    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log
    success_teardown
}

@test "start gregd with filled config file" {
    export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}/filled
    mkdir -p ${XDG_CONFIG_HOME}/gregd
    echo "Changed XDG_CONFIG_HOME:"
    echo "export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}"
    cp e2e-tests/test-config.yaml ${XDG_CONFIG_HOME}/gregd/services.yaml
    rm -f /tmp/redir-test.std*
    rm -f /tmp/env_out_test.out
    catch_output gregd &
    sleep .01


    regex_file_check "^ *9$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)
    regex_file_check "^ *9$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)

    diff /tmp/redir-test.stdout <(echo "foobar") || (echo "incorrect output for redir test. file content of /tmp/redir-test.stdout: $(cat -e /tmp/redir-test.stdout)"; false)
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.* Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Starting 2 processes for catnull\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Starting 1 processes for sleepone\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Starting 1 processes for redir_output\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Starting 1 processes for env_test\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Process [0-9]+ from service catnull died with exit code 0.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log


    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Starting 2 processes for catnull\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Starting 1 processes for sleepone\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log

    sleep 0.1
    pkill -15 gregd
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Process [0-9]+ from service sleepone died with exit code 0.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*: Initiating shutdown.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Initiating shutdown$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*: No child processes currently alive, shutting down.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: No child processes currently alive, shutting down$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^ *12$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)
    regex_file_check "^ *12$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)
    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log

    regex_file_check "^E2E_TEST_VAR=testvalue" /tmp/env_out_test.out

    success_teardown
}

@test "test kill_orphans" {
    export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}/orphans
    mkdir -p ${XDG_CONFIG_HOME}/gregd
    echo "Changed XDG_CONFIG_HOME:"
    echo "export XDG_CONFIG_HOME=${XDG_CONFIG_HOME}"
    cp e2e-tests/orphan-test-config.yaml ${XDG_CONFIG_HOME}/gregd/services.yaml
    catch_output gregd &
    sleep .01


    regex_file_check "^ *2$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)
    regex_file_check "^ *2$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.* Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Starting 1 processes for orphan_test\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log


    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Listening on ${XDG_RUNTIME_DIR}/gregd.sock\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Starting 1 processes for orphan_test\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log

    echo "{}" > ${XDG_CONFIG_HOME}/gregd/services.yaml
    pkill -1 gregd
    regex_file_check "^ *5$" <(cat ${TEST_OUTPUT_DIR}/gregd_stderr.log | wc -l)
    regex_file_check "^ *5$" <(cat ${XDG_STATE_HOME}/gregd/gregd.log | wc -l)

    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd.*:.* Received SIGHUP, reloading config.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Killing orphaned process [0-9]+ of non-existant service orphan_test\.\.\..*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log
    regex_file_check "^.*${TIMESTAMP_REGEX}.*gregd::supervisor.*:.* Process [0-9]+ from service orphan_test died by signal.*$" ${TEST_OUTPUT_DIR}/gregd_stderr.log

    regex_file_check "^${TIMESTAMP_REGEX} gregd info: Received SIGHUP, reloading config$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Killing orphaned process [0-9]+ of non-existant service orphan_test\.\.\.$" ${XDG_STATE_HOME}/gregd/gregd.log
    regex_file_check "^${TIMESTAMP_REGEX} gregd::supervisor info: Process [0-9]+ from service orphan_test died by signal$" ${XDG_STATE_HOME}/gregd/gregd.log




    pkill -15 gregd

    is_empty_file ${TEST_OUTPUT_DIR}/gregd_stdout.log

    success_teardown
}
