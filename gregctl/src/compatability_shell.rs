use crate::{constant, executor};
use std::io::{self, BufRead};

pub fn compatability_shell() -> i32 {
    for line in io::stdin().lock().lines() {
        match line {
            Err(e) => {
                eprintln!("error: {}", e);
            }
            Ok(line) => {
                let args: Vec<String> = line.split(" ").map(String::from).collect();
                match executor::execute_command(&args) {
                    Ok(output) => {
                        print!("{}", output);
                    }
                    Err(error) => {
                        eprintln!("{}: {}", constant::PROGRAM_NAME, error);
                    }
                }
            }
        }
    }
    return 0;
}
