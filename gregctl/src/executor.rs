use std::{error, fmt};

use libgreg::connection;
use libgreg::protocol::{self, Message};

pub fn execute_command(args: &[String]) -> Result<String, Error> {
    match parse_command(args) {
        Ok(request) => match send_request(request) {
            Ok(response) => handle_response(response),
            Err(e) => Err(Error::ConnectError(e)),
        },
        Err(e) => Err(e),
    }
}

fn parse_command(args: &[String]) -> Result<protocol::Request, Error> {
    match args[0].as_ref() {
        "ping" => Ok(protocol::Request::Ping),
        "log" => {
            if args.len() == 2 {
                Ok(protocol::Request::LogMessage {
                    message: args[1].clone(),
                })
            } else {
                Err(Error::InvalidUsage(format!("log <message>")))
            }
        }
        "reload" => Ok(protocol::Request::Reload),
        "status" => match args.len() {
            1 => Ok(protocol::Request::Status { service: None }),
            2 => Ok(protocol::Request::Status {
                service: Some(args[1].clone()),
            }),
            _ => Err(Error::InvalidUsage(format!("status [<service>]"))),
        },
        "start" => {
            if args.len() == 2 {
                Ok(protocol::Request::Start {
                    service: args[1].clone(),
                })
            } else {
                Err(Error::InvalidUsage(format!("start <service>")))
            }
        }
        "stop" => {
            if args.len() == 2 {
                Ok(protocol::Request::Stop {
                    service: args[1].clone(),
                })
            } else {
                Err(Error::InvalidUsage(format!("stop <service>")))
            }
        }
        "restart" => {
            if args.len() == 2 {
                Ok(protocol::Request::Restart {
                    service: args[1].clone(),
                })
            } else {
                Err(Error::InvalidUsage(format!("restart <service>")))
            }
        }
        "shutdown" => Ok(protocol::Request::Shutdown),
        _ => Err(Error::InvalidCmd(args[0].clone())),
    }
}

fn send_request(request: protocol::Request) -> Result<Vec<u8>, connection::Error> {
    let bind_path = connection::path();
    let mut socket_link = connection::SocketLink::new(&bind_path);
    let stream = socket_link.connect()?;
    let mut connection = connection::Connection::new(&stream);

    connection.write_message(&request.serialize())?;
    let raw_response = connection.read_message(protocol::DELIM)?;
    Ok(raw_response)
}

fn human_status_of_services(services: &[protocol::ServiceInfo]) -> String {
    services.iter().fold(String::new(), |acc, service| {
        let service_line = format!("{} ({})\n", service.name, service.status);
        let process_lines = service
            .processes
            .iter()
            .fold(String::new(), |acc, process| {
                acc + format!("    {:<8} {}\n", process.pid, process.status).as_ref()
            });
        acc + service_line.as_ref() + process_lines.as_ref()
    })
}

fn handle_response(raw_response: Vec<u8>) -> Result<String, Error> {
    match protocol::Response::deserialize(&raw_response) {
        Ok(protocol::Response::Ok) => Ok("".into()),
        Ok(protocol::Response::Status { services }) => {
            if services.len() == 0 {
                Err(Error::ErrorResponse(format!("No services found")))
            } else {
                Ok(human_status_of_services(services.as_ref()))
            }
        },
        Ok(protocol::Response::ConfigError { message }) => {
            Err(Error::ErrorResponse(format!("Could not reload config: {}\n", message)))
        }
        Ok(protocol::Response::BadRequest) => {
            Err(Error::ErrorResponse(format!("Server indicated \"Bad Request\". Please check you are running a compatible version of gregd.\n")))
        }
        Ok(protocol::Response::UnknownService { service }) => {
            Err(Error::ErrorResponse(format!("Unknown service {}, maybe you need to reload the config?", service)))
        }
        Ok(protocol::Response::ShuttingDown) => {
            Err(Error::ErrorResponse(format!("Request could not be completed: gregd is shutting down.")))
        }
        Err(e) => {
            Err(Error::ParseError(e))
        }
    }
}

#[derive(Debug)]
pub enum Error {
    InvalidUsage(String),
    ConnectError(connection::Error),
    InvalidCmd(String),
    ErrorResponse(String),
    ParseError(protocol::Error),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::InvalidUsage(correct_usage) => {
                write!(f, "usage: {}", correct_usage)
            }
            Error::ConnectError(ref e) => write!(f, "Failed to send request: {}", e),
            Error::InvalidCmd(command) => write!(f, "Invalid command: {}", command),
            Error::ErrorResponse(message) => write!(f, "{}", message),
            Error::ParseError(e) => write!(f, "Could not parse response: {}", e),
        }
    }
}
