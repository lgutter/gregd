use std::env;
use std::process::exit;

mod compatability_shell;
mod constant;
mod executor;

fn program() -> i32 {
    let args: Vec<String> = env::args().collect();

    if args.len() >= 2 {
        match executor::execute_command(&args[1..]) {
            Ok(output) => {
                print!("{}", output);
                return 0;
            }
            Err(error) => {
                eprintln!("{}: {}", constant::PROGRAM_NAME, error);
                return 1;
            }
        }
    } else {
        eprintln!("WARNING: this shell is only here for evaluation compatability.\nInput will only be split on spaces. Please use command line arguments instead.");
        compatability_shell::compatability_shell()
    }
}

fn main() {
    exit(program())
}
