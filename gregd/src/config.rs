use serde::{Deserialize, Serialize};
use serde_yaml;
use std::collections::HashMap;
use std::error;
use std::ffi::CString;
use std::fs;
use std::path::PathBuf;
use std::{env, fmt, io};

/// Types of operating system signals
#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone, Copy)]
#[repr(i32)]
#[non_exhaustive]
pub enum Signal {
    /// Hangup
    SIGHUP = libc::SIGHUP,
    /// Interrupt
    SIGINT = libc::SIGINT,
    /// Quit
    SIGQUIT = libc::SIGQUIT,
    /// Illegal instruction (not reset when caught)
    SIGILL = libc::SIGILL,
    /// Trace trap (not reset when caught)
    SIGTRAP = libc::SIGTRAP,
    /// Abort
    SIGABRT = libc::SIGABRT,
    /// Bus error
    SIGBUS = libc::SIGBUS,
    /// Floating point exception
    SIGFPE = libc::SIGFPE,
    /// Kill (cannot be caught or ignored)
    SIGKILL = libc::SIGKILL,
    /// User defined signal 1
    SIGUSR1 = libc::SIGUSR1,
    /// Segmentation violation
    SIGSEGV = libc::SIGSEGV,
    /// User defined signal 2
    SIGUSR2 = libc::SIGUSR2,
    /// Write on a pipe with no one to read it
    SIGPIPE = libc::SIGPIPE,
    /// Alarm clock
    SIGALRM = libc::SIGALRM,
    /// Software termination signal from kill
    SIGTERM = libc::SIGTERM,
    /// Stack fault (obsolete)
    #[cfg(all(
        any(
            target_os = "android",
            target_os = "emscripten",
            target_os = "fuchsia",
            target_os = "linux"
        ),
        not(any(target_arch = "mips", target_arch = "mips64", target_arch = "sparc64"))
    ))]
    SIGSTKFLT = libc::SIGSTKFLT,
    /// To parent on child stop or exit
    SIGCHLD = libc::SIGCHLD,
    /// Continue a stopped process
    SIGCONT = libc::SIGCONT,
    /// Sendable stop signal not from tty
    SIGSTOP = libc::SIGSTOP,
    /// Stop signal from tty
    SIGTSTP = libc::SIGTSTP,
    /// To readers pgrp upon background tty read
    SIGTTIN = libc::SIGTTIN,
    /// Like TTIN if (tp->t_local&LTOSTOP)
    SIGTTOU = libc::SIGTTOU,
    /// Urgent condition on IO channel
    SIGURG = libc::SIGURG,
    /// Exceeded CPU time limit
    SIGXCPU = libc::SIGXCPU,
    /// Exceeded file size limit
    SIGXFSZ = libc::SIGXFSZ,
    /// Virtual time alarm
    SIGVTALRM = libc::SIGVTALRM,
    /// Profiling time alarm
    SIGPROF = libc::SIGPROF,
    /// Window size changes
    SIGWINCH = libc::SIGWINCH,
    /// Input/output possible signal
    SIGIO = libc::SIGIO,
    #[cfg(any(
        target_os = "android",
        target_os = "emscripten",
        target_os = "fuchsia",
        target_os = "linux"
    ))]
    /// Power failure imminent.
    SIGPWR = libc::SIGPWR,
    /// Bad system call
    SIGSYS = libc::SIGSYS,
    #[cfg(not(any(
        target_os = "android",
        target_os = "emscripten",
        target_os = "fuchsia",
        target_os = "linux",
        target_os = "redox"
    )))]
    /// Emulator trap
    SIGEMT = libc::SIGEMT,
    #[cfg(not(any(
        target_os = "android",
        target_os = "emscripten",
        target_os = "fuchsia",
        target_os = "linux",
        target_os = "redox"
    )))]
    /// Information request
    SIGINFO = libc::SIGINFO,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
#[serde(rename_all = "kebab-case")]
pub enum AutoRestart {
    Always,
    OnError,
    Never,
}

// We use this wrapper around CString in order to be able to make sure a key
// for env is always valid for use with env::set_var, so it can not panic. since
// it is a CString, it can not contain '\0', and we manually check if the CString
// contains '=' or is empty, the only other 2 cases that would make env::set_var panic.
#[derive(Serialize, PartialEq, Eq, Debug, Clone, Hash)]
pub struct EnvKey(pub CString);

impl<'de> Deserialize<'de> for EnvKey {
    fn deserialize<D>(deserializer: D) -> Result<EnvKey, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let s: CString = serde::Deserialize::deserialize(deserializer)?;
        if s.as_bytes().len() == 0 {
            return Err(serde::de::Error::custom("EnvKey cannot be empty"));
        } else if s.as_bytes().contains(&61) {
            return Err(serde::de::Error::custom("EnvKey cannot contain '='"));
        }
        Ok(EnvKey(s))
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
#[serde(deny_unknown_fields)]
pub struct Service {
    pub command: CString,
    #[serde(default = "default_args")]
    pub args: Vec<CString>,
    #[serde(default = "default_replicas")]
    pub replicas: u32,
    #[serde(default = "default_umask", skip_serializing_if = "Option::is_none")]
    pub umask: Option<libc::mode_t>,
    #[serde(
        default = "default_workingdir",
        skip_serializing_if = "Option::is_none"
    )]
    pub workingdir: Option<CString>,
    pub autostart: bool,
    #[serde(default = "default_autorestart")]
    pub autorestart: AutoRestart,
    #[serde(default = "default_exitcodes")]
    pub exitcodes: Vec<i8>,
    #[serde(default = "default_startretries")]
    pub startretries: u32,
    #[serde(default = "default_starttime_ms")]
    pub starttime_ms: u64,
    #[serde(default = "default_stopsignal")]
    pub stopsignal: Signal,
    #[serde(default = "default_stoptime_ms")]
    pub stoptime_ms: u64,
    #[serde(default = "default_outfile")]
    pub stdout: CString,
    #[serde(default = "default_outfile")]
    pub stderr: CString,
    #[serde(default = "default_env")]
    pub env: HashMap<EnvKey, CString>,
}

fn default_args() -> Vec<CString> {
    Vec::new()
}

fn default_replicas() -> u32 {
    1
}

fn default_umask() -> Option<libc::mode_t> {
    None // inherit umask
}

fn default_workingdir() -> Option<CString> {
    None // inherit workingdir
}

fn default_autorestart() -> AutoRestart {
    AutoRestart::OnError
}

fn default_exitcodes() -> Vec<i8> {
    Vec::from([0])
}

fn default_startretries() -> u32 {
    3
}

fn default_starttime_ms() -> u64 {
    5_000
}

fn default_stopsignal() -> Signal {
    Signal::SIGTERM
}

fn default_stoptime_ms() -> u64 {
    10_000
}

fn default_outfile() -> CString {
    CString::new("/dev/null").expect("CString::new can not fail")
}

fn default_env() -> HashMap<EnvKey, CString> {
    HashMap::new()
}

pub fn path() -> PathBuf {
    let path_string = match env::var("XDG_CONFIG_HOME") {
        Ok(dir) => format!("{}/gregd/services.yaml", dir),
        Err(_) => match env::var("HOME") {
            Ok(dir) => format!("{}/.config/gregd/services.yaml", dir),
            Err(_) => String::from("services.yaml"),
        },
    };

    PathBuf::from(path_string)
}

pub fn load(path: PathBuf) -> Result<Config, Error> {
    let mut reader = match fs::File::open(&path) {
        Err(e) => return Err(Error::OpenError(path, e)),
        Ok(reader) => reader,
    };
    let config = Config::deserialize(&mut reader).map_err(Error::ParsingError);
    debug!("loaded config file {:?}", path);
    return config;
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug, Clone)]
pub struct Config(pub HashMap<String, Service>);

impl Config {
    #[allow(dead_code)]
    pub fn serialize(&self) -> String {
        serde_yaml::to_string(self).expect("Should be able to serialize")
    }

    pub fn deserialize(reader: &mut dyn io::Read) -> Result<Config, serde_yaml::Error> {
        serde_yaml::from_reader(reader)
    }
}

#[derive(Debug)]
pub enum Error {
    OpenError(PathBuf, io::Error),
    ParsingError(serde_yaml::Error),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::OpenError(ref p, e) => write!(f, "Could not open file {:?}: {}", p, e),
            Error::ParsingError(ref e) => write!(f, "Parsing error: {}", e),
        }
    }
}

impl From<Error> for String {
    fn from(error: Error) -> Self {
        format!("{}", error)
    }
}

#[cfg(test)]
mod tests {
    use std::{collections::HashMap, path::PathBuf};

    use super::{load, Config, Error};
    #[test]
    fn load_error_invalid_path() {
        let path = PathBuf::from("foo/bar");
        match load(path) {
            Err(Error::OpenError(..)) => return,
            Ok(_) => panic!("should error"),
            Err(_) => panic!("should error differently"),
        }
    }

    #[test]
    fn serialize_config() {
        let config = Config(HashMap::new());
        assert_eq!(config.serialize(), "---\n{}\n")
    }
}
