use std::env;
use std::fmt;
use std::fs;
use std::io::Write;
use std::path::PathBuf;
use std::process::exit;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::sync::Once;
use std::thread;
use std::time::SystemTime;

use chrono::prelude::{DateTime, Utc};

#[derive(PartialEq, PartialOrd, Eq)]
pub enum Level {
    Debug,
    Info,
    Warn,
    Error,
    Fatal,
}

impl fmt::Display for Level {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let str = match self {
            &Level::Debug => "debug",
            &Level::Info => "info",
            &Level::Warn => "warn",
            &Level::Error => "error",
            &Level::Fatal => "fatal",
        };
        write!(f, "{}", str)
    }
}

impl Level {
    fn from_env() -> Option<Level> {
        match env::var("GREGD_LOG_LEVEL") {
            Ok(str) => match str.as_str() {
                "debug" => Some(Level::Debug),
                "info" => Some(Level::Info),
                "warn" => Some(Level::Warn),
                "error" => Some(Level::Error),
                "fatal" => Some(Level::Fatal),
                _ => None,
            },
            Err(_) => Some(Level::Info),
        }
    }

    fn escape_prefix(&self) -> &str {
        match self {
            &Level::Debug => "\x1b[3m",    // italic
            &Level::Info => "",            // no color
            &Level::Warn => "\x1b[1m",     // bold
            &Level::Error => "\x1b[0;31m", // red
            &Level::Fatal => "\x1b[1;31m", // bold red
        }
    }
}

enum OutputMode {
    CSISequence,
    Plain,
}

struct Message {
    time: SystemTime,
    level: Level,
    module_path: String,
    contents: String,
}

impl Message {
    fn new(time: SystemTime, level: Level, module_path: &str, contents: &str) -> Message {
        Message {
            time,
            level,
            module_path: module_path.into(),
            contents: contents.into(),
        }
    }

    fn format(&self, output_mode: OutputMode) -> String {
        let Message {
            time,
            level,
            module_path,
            contents,
        } = self;

        let dt: DateTime<Utc> = time.clone().into();
        let formatted_time = dt.format("%Y-%m-%d %H:%M:%S");

        match output_mode {
            OutputMode::CSISequence => format!(
                "\x1b[32m[{}]\x1b[0m \x1b[0;33m{}\x1b[0m: {}{}\x1b[0m",
                formatted_time,
                module_path,
                level.escape_prefix(),
                contents,
            ),
            OutputMode::Plain => format!(
                "[{}] {} {}: {}",
                formatted_time, module_path, level, contents,
            ),
        }
    }
}

fn log_path() -> PathBuf {
    let path_string = match env::var("XDG_STATE_HOME") {
        Ok(dir) => format!("{}/gregd/gregd.log", dir),
        Err(_) => match env::var("HOME") {
            Ok(dir) => format!("{}/.local/state/gregd/gregd.log", dir),
            Err(_) => String::from("gregd.log"),
        },
    };

    PathBuf::from(path_string)
}

fn prepare_listener(tx: Sender<Message>) -> (Level, Option<fs::File>) {
    let log_level = match Level::from_env() {
        Some(level) => level,
        None => {
            tx.send(Message::new(
                SystemTime::now(),
                Level::Warn,
                module_path!(),
                &format!("GREGD_LOG_LEVEL is set to an invalid value, assuming 'info'"),
            ))
            .unwrap();
            Level::Info
        }
    };

    let log_path = log_path();
    if let Some(parent_dir) = log_path.parent() {
        // Errors captured when trying to open file.
        let _ = fs::create_dir_all(parent_dir);
    }

    let file_result = fs::File::options()
        .append(true)
        .create(true)
        .open(&log_path);

    match file_result {
        Ok(file) => {
            tx.send(Message::new(
                SystemTime::now(),
                Level::Debug,
                module_path!(),
                &format!(
                    "Logging thread started with log file {}",
                    log_path.display()
                ),
            ))
            .unwrap();

            (log_level, Some(file))
        }
        Err(err) => {
            tx.send(Message::new(
                SystemTime::now(),
                Level::Error,
                module_path!(),
                &format!(
                    "Logging thread started, but error opening log file {}: {}",
                    log_path.display(),
                    err
                ),
            ))
            .unwrap();

            (log_level, None)
        }
    }
}

fn listen_for_messages(rx: Receiver<Message>, log_level: Level, mut file: Option<fs::File>) {
    for msg in rx.iter() {
        if msg.level < log_level {
            continue;
        }

        eprintln!("{}", msg.format(OutputMode::CSISequence));

        if let Some(ref mut f) = file {
            if let Err(err) = writeln!(f, "{}", msg.format(OutputMode::Plain)) {
                eprintln!(
                    "{}",
                    Message::new(
                        SystemTime::now(),
                        Level::Error,
                        module_path!(),
                        &format!("Error writing to log file: {}", err),
                    )
                    .format(OutputMode::CSISequence),
                )
            }
        }

        if msg.level == Level::Fatal {
            exit(1);
        }
    }
}

// Gets the channel tx for the logger. Any messages written to this channel will
// be printed out by a dedicated thread. If no channel exists yet, the channel
// will be created and a thread to handle the channel will be spawned.
fn get_channel_tx() -> Sender<Message> {
    static mut CHANNEL_TX: Option<Sender<Message>> = None;
    static CHANNEL_TX_INIT: std::sync::Once = Once::new();

    // Adapted from call_once example code, which also uses unsafe blocks.
    CHANNEL_TX_INIT.call_once(|| {
        let (tx, rx): (Sender<Message>, Receiver<Message>) = channel();

        let loopback_tx = tx.clone();
        thread::spawn(move || {
            let (log_level, file) = prepare_listener(loopback_tx);
            listen_for_messages(rx, log_level, file)
        });

        // This is safe because call_once will ensure this is only called once
        // and only from a single thread. Any other threads would wait for the
        // call_once call to finish before continuing.
        unsafe {
            CHANNEL_TX = Some(tx);
        }
    });

    // This is safe because, once the above call_once function was executed,
    // CHANNEL_TX is never written to again and therefore can't change while
    // trying to use it.
    unsafe { return CHANNEL_TX.clone().unwrap() }
}

// log_internal should never be called directly, and should be called via one
// of the log macros instead. It is public to allow the log macros to be called
// from outside of this module.
#[doc(hidden)]
pub fn log_internal(level: Level, module_path: &str, contents: &str) {
    let tx = get_channel_tx();

    let msg = Message::new(SystemTime::now(), level, module_path, contents);

    if let Err(e) = tx.send(msg) {
        panic!("log channel closed: {}", e);
    }
}

macro_rules! log {
    ($level:expr, $($fmt_args:tt)*) => {
        {
            let msg = format!($($fmt_args)*);
            $crate::log::log_internal($level, module_path!(), &msg);
            if ($level == $crate::log::Level::Fatal) {
                std::thread::sleep(std::time::Duration::from_millis(200));
                panic!("Did not die after fatal log. Was called with: {}", msg);
            };
        }
    };
}

macro_rules! debug {
    ($($fmt_args:tt)*) => {
        log!($crate::log::Level::Debug, $($fmt_args)*)
    };
}

macro_rules! info {
    ($($fmt_args:tt)*) => {
        log!($crate::log::Level::Info, $($fmt_args)*)
    };
}

macro_rules! warn {
    ($($fmt_args:tt)*) => {
        log!($crate::log::Level::Warn, $($fmt_args)*)
    };
}

macro_rules! error {
    ($($fmt_args:tt)*) => {
        log!($crate::log::Level::Error, $($fmt_args)*)
    };
}

macro_rules! fatal {
    ($($fmt_args:tt)*) => {
        {
            log!($crate::log::Level::Fatal, $($fmt_args)*);
            unreachable!()
        }
    };
}

#[cfg(test)]
mod tests {
    use std::time::SystemTime;

    use super::{Level, Message, OutputMode};

    #[test]
    fn format_message_debug() {
        let msg = Message::new(
            SystemTime::UNIX_EPOCH,
            Level::Debug,
            "path::to::module",
            "Something happened",
        );

        assert_eq!(
            msg.format(OutputMode::Plain),
            "[1970-01-01 00:00:00] path::to::module debug: Something happened"
        );
        assert_eq!(msg.format(OutputMode::CSISequence), "\x1b[32m[1970-01-01 00:00:00]\x1b[0m \x1b[0;33mpath::to::module\x1b[0m: \x1b[3mSomething happened\x1b[0m");
    }

    #[test]
    fn format_message_info() {
        let msg = Message::new(
            SystemTime::UNIX_EPOCH,
            Level::Info,
            "path::to::module",
            "Something happened",
        );

        assert_eq!(
            msg.format(OutputMode::Plain),
            "[1970-01-01 00:00:00] path::to::module info: Something happened"
        );
        assert_eq!(msg.format(OutputMode::CSISequence), "\x1b[32m[1970-01-01 00:00:00]\x1b[0m \x1b[0;33mpath::to::module\x1b[0m: Something happened\x1b[0m");
    }

    #[test]
    fn format_message_warn() {
        let msg = Message::new(
            SystemTime::UNIX_EPOCH,
            Level::Warn,
            "path::to::module",
            "Something happened",
        );

        assert_eq!(
            msg.format(OutputMode::Plain),
            "[1970-01-01 00:00:00] path::to::module warn: Something happened"
        );
        assert_eq!(msg.format(OutputMode::CSISequence), "\x1b[32m[1970-01-01 00:00:00]\x1b[0m \x1b[0;33mpath::to::module\x1b[0m: \x1b[1mSomething happened\x1b[0m");
    }

    #[test]
    fn format_message_error() {
        let msg = Message::new(
            SystemTime::UNIX_EPOCH,
            Level::Error,
            "path::to::module",
            "Something happened",
        );

        assert_eq!(
            msg.format(OutputMode::Plain),
            "[1970-01-01 00:00:00] path::to::module error: Something happened"
        );
        assert_eq!(msg.format(OutputMode::CSISequence), "\x1b[32m[1970-01-01 00:00:00]\x1b[0m \x1b[0;33mpath::to::module\x1b[0m: \x1b[0;31mSomething happened\x1b[0m");
    }

    #[test]
    fn format_message_fatal() {
        let msg = Message::new(
            SystemTime::UNIX_EPOCH,
            Level::Fatal,
            "path::to::module",
            "Something happened",
        );

        assert_eq!(
            msg.format(OutputMode::Plain),
            "[1970-01-01 00:00:00] path::to::module fatal: Something happened"
        );
        assert_eq!(msg.format(OutputMode::CSISequence), "\x1b[32m[1970-01-01 00:00:00]\x1b[0m \x1b[0;33mpath::to::module\x1b[0m: \x1b[1;31mSomething happened\x1b[0m");
    }
}
