use libgreg::connection;
use libgreg::protocol::{self, Message};
use signal_hook::iterator::Signals;
use std::thread;
use std::{io, process};

#[macro_use]
mod log;
mod config;
mod supervisor;
mod syscall;

fn handle_request(
    request: protocol::Request,
    supervisor_bridge: &supervisor::Bridge,
) -> protocol::Response {
    match request {
        protocol::Request::Ping => {
            debug!("Received ping request");
            protocol::Response::Ok
        }
        protocol::Request::LogMessage { message } => {
            info!("Message from client: {}", message);
            protocol::Response::Ok
        }
        protocol::Request::Reload => match config::load(config::path()) {
            Err(e) => {
                error!("Could not reload config: {}", e);
                protocol::Response::ConfigError { message: e.into() }
            }
            Ok(config) => match supervisor_bridge.load_config(config) {
                Ok(()) => {
                    info!("Reloaded config as requested through socket");
                    protocol::Response::Ok
                }
                Err(e) => e.into(),
            },
        },
        protocol::Request::Status { service } => match supervisor_bridge.status(service.clone()) {
            Ok(services) => {
                match service {
                    Some(service) => info!("Got status for service {}", service),
                    None => info!("Got status for all services"),
                }
                protocol::Response::Status { services }
            }
            Err(e) => e.into(),
        },
        protocol::Request::Start { service } => match supervisor_bridge.start(&service) {
            Ok(()) => protocol::Response::Ok,
            Err(e) => e.into(),
        },
        protocol::Request::Stop { service } => match supervisor_bridge.stop(&service) {
            Ok(()) => protocol::Response::Ok,
            Err(e) => e.into(),
        },
        protocol::Request::Restart { service } => match supervisor_bridge.restart(&service) {
            Ok(()) => protocol::Response::Ok,
            Err(e) => e.into(),
        },
        protocol::Request::Shutdown => {
            // Shutdown is implemented by simply sending a SIGTERM to ourselves.
            // We catch it in the main, which allows us to cleanup and exit the
            // program gracefully.
            if let Err(e) = syscall::kill(process::id() as libc::pid_t, libc::SIGTERM) {
                fatal!("Unable to kill self: {}", e);
            }
            protocol::Response::Ok
        }
    }
}

fn handle_client(mut connection: connection::Connection, supervisor_bridge: supervisor::Bridge) {
    loop {
        let serialized_request = match connection.read_message(protocol::DELIM) {
            Ok(serialized_request) => {
                if serialized_request.len() == 0 {
                    break;
                } else {
                    serialized_request
                }
            }
            Err(e) => {
                warn!("Connection to client broke: {}", e);
                break;
            }
        };

        let response = match protocol::Request::deserialize(&serialized_request) {
            Err(e) => {
                warn!("Unable to parse request: {}", e);

                protocol::Response::BadRequest
            }
            Ok(request) => handle_request(request, &supervisor_bridge),
        };

        if let Err(e) = connection.write_message(&response.serialize()) {
            warn!("Unable to write response to client: {}", e)
        }
    }
}

fn main() -> Result<(), io::Error> {
    let term_signals = Signals::new(&[
        signal_hook::SIGINT,
        signal_hook::SIGTERM,
        signal_hook::SIGHUP,
    ])?;

    let bind_path = connection::path();
    let mut socket_link = connection::SocketLink::new(&bind_path);

    let config = match config::load(config::path()) {
        Err(e) => fatal!("Could not load config: {}", e),
        Ok(config) => config,
    };

    let listener = match socket_link.bind() {
        Ok(listener) => {
            info!("Listening on {}...", bind_path.display());
            listener
        }
        Err(e) => fatal!("{}", e),
    };

    let supervisor_bridge = supervisor::start();
    let signal_bridge = supervisor_bridge.clone();

    supervisor_bridge
        .load_config(config)
        .expect("load_config cannot fail as supervisor cannot be shutting down at this stage");

    thread::spawn(move || {
        for stream in listener.incoming() {
            debug!("New connection");
            match stream {
                Ok(stream) => {
                    let supervisor_bridge = supervisor_bridge.clone();
                    thread::spawn(move || {
                        handle_client(connection::Connection::new(&stream), supervisor_bridge)
                    });
                }
                Err(e) => {
                    error!("Failed to read from unix socket: {}", e);
                    return;
                }
            }
        }
    });

    for signal in term_signals.forever() {
        match signal {
            signal_hook::SIGINT | signal_hook::SIGTERM => {
                debug!("Received signal {}", signal);
                match signal_bridge.shutdown() {
                    Ok(()) => return Ok(()),
                    Err(e) => fatal!("Unable to initiate shutdown: {}", e),
                }
            }
            signal_hook::SIGHUP => {
                info!("Received SIGHUP, reloading config");
                match config::load(config::path()) {
                    Err(e) => error!("Could not reload config: {}", e),
                    Ok(config) => {
                        if let Err(e) = signal_bridge.load_config(config) {
                            // This is fatal because we expect it to be
                            // impossible. signal_bridge.load_config can only
                            // fail if we are currently shutting down, but we
                            // won't ever get here in that case since
                            // signal_bridge.shutdown is blocking.
                            fatal!("Could not reload config: {}", e)
                        }
                    }
                }
            }
            _ => unreachable!(),
        }
    }

    unreachable!()
}
