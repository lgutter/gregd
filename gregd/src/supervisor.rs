use std::collections::HashMap;
use std::error;
use std::ffi::{CStr, CString};
use std::fmt;
use std::mem;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::thread;
use std::time;

use libgreg::protocol;

use crate::config;
use crate::syscall;

pub fn start() -> Bridge {
    Bridge(Supervisor::spawn())
}

struct Notify<T>(Sender<Result<T, Error>>);

impl<T> Notify<T> {
    fn send(self, result: Result<T, Error>) {
        self.0
            .send(result)
            .expect("channel closed before answer was sent")
    }

    fn accept(self, answer: T) {
        self.0
            .send(Ok(answer))
            .expect("channel closed before answer was sent");
    }

    fn reject(self, e: Error) {
        self.0
            .send(Err(e))
            .expect("channel closed before answer was sent");
    }
}

enum InternalEvent {
    ProcessDied {
        pid: libc::pid_t,
        stat_loc: syscall::StatLoc,
    },
    MurderProcess {
        pid: libc::pid_t,
    },
}

enum ExternalEvent {
    LoadConfig {
        config: config::Config,
        notify: Notify<()>,
    },
    ServiceStatus {
        service: Option<String>,
        notify: Notify<Vec<protocol::ServiceInfo>>,
    },
    Start {
        service: String,
        notify: Notify<()>,
    },
    Stop {
        service: String,
        notify: Notify<()>,
    },
    Restart {
        service: String,
        notify: Notify<()>,
    },
    Shutdown {
        notify: Notify<()>,
    },
}

impl ExternalEvent {
    fn reject(self, e: Error) {
        match self {
            Self::LoadConfig { notify, .. } => notify.reject(e),
            Self::ServiceStatus { notify, .. } => notify.reject(e),
            Self::Start { notify, .. } => notify.reject(e),
            Self::Stop { notify, .. } => notify.reject(e),
            Self::Restart { notify, .. } => notify.reject(e),
            Self::Shutdown { notify, .. } => notify.reject(e),
        }
    }
}

enum Event {
    Internal(InternalEvent),
    External(ExternalEvent),
}

#[derive(Debug, PartialEq, Eq, Clone)]
struct Process {
    command: CString,
    args: Vec<CString>,
    umask: Option<libc::mode_t>,
    workingdir: Option<CString>,
    stdout: CString,
    stderr: CString,
    env: HashMap<config::EnvKey, CString>,
    service: String,
    start_moment: time::Instant,
    kill_pending: bool,
    retries_left: u32,
}

impl Process {
    fn matches_model(&self, service: &config::Service) -> bool {
        self.command == service.command
            && self.args == service.args
            && self.umask == service.umask
            && self.workingdir == service.workingdir
            && self.stdout == service.stdout
            && self.stderr == service.stderr
            && self.env == service.env
    }

    fn status(&self, service_config: &config::Service) -> protocol::ProcessStatus {
        if self.kill_pending {
            protocol::ProcessStatus::Terminating
        } else if self.start_moment.elapsed()
            < time::Duration::from_millis(service_config.starttime_ms)
        {
            protocol::ProcessStatus::Starting
        } else {
            protocol::ProcessStatus::Running
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq)]
enum Wanted {
    Alive,
    Dead,
}

struct Service {
    wanted: Wanted,
    name: String,
    config: config::Service,
}

impl Service {
    fn did_change(&self, services: &HashMap<String, Service>) -> bool {
        match services.get(&self.name) {
            None => return true,
            Some(service) => {
                return self.config.command != service.config.command
                    || self.config.args != service.config.args
                    || self.config.replicas != service.config.replicas
                    || self.config.umask != service.config.umask
                    || self.config.workingdir != service.config.workingdir
                    || self.config.stdout != service.config.stdout
                    || self.config.stderr != service.config.stderr
                    || self.config.env != service.config.env
                    || self.wanted != service.wanted
            }
        }
    }

    fn status(&self, processes: &HashMap<libc::pid_t, Process>) -> protocol::ServiceStatus {
        let amount_wanted_alive = self.config.replicas;
        let amount_alive = processes
            .values()
            .filter(|process| process.service == self.name && !process.kill_pending)
            .count() as u32;

        // Because we don't count processes who are kill pending as alive
        // processes, it is impossible that we have more alive processes than
        // wanted alive processes.
        assert!(amount_wanted_alive >= amount_alive);

        match (self.wanted, amount_wanted_alive - amount_alive) {
            (Wanted::Dead, _) => protocol::ServiceStatus::Inactive,
            (Wanted::Alive, 0) => protocol::ServiceStatus::Active,
            (Wanted::Alive, 1..) => protocol::ServiceStatus::Degraded,
        }
    }
}

enum SupervisorStatus {
    Running,
    ShuttingDown { notify: Notify<()> },
    Down,
}

struct Supervisor {
    status: SupervisorStatus,
    event_queue: Sender<Event>,
    services: HashMap<String, Service>,
    processes: HashMap<libc::pid_t, Process>,
}

impl Supervisor {
    fn spawn() -> Sender<Event> {
        let (sender, receiver) = channel();
        let event_queue = sender.clone();
        thread::spawn(move || {
            Supervisor {
                status: SupervisorStatus::Running,
                event_queue,
                services: HashMap::new(),
                processes: HashMap::new(),
            }
            .listen(receiver)
        });
        sender
    }

    fn listen(&mut self, recv: Receiver<Event>) {
        for msg in recv.iter() {
            match msg {
                Event::Internal(InternalEvent::ProcessDied { pid, stat_loc }) => {
                    self.handle_process_died(pid, stat_loc)
                }
                Event::Internal(InternalEvent::MurderProcess { pid }) => {
                    self.handle_murder_process(pid)
                }
                Event::External(external_event) => match self.status {
                    SupervisorStatus::Running => match external_event {
                        ExternalEvent::LoadConfig { config, notify } => {
                            notify.accept(self.handle_load_config(config))
                        }
                        ExternalEvent::ServiceStatus { service, notify } => {
                            notify.send(self.handle_service_status(service))
                        }
                        ExternalEvent::Start { service, notify } => {
                            notify.send(self.handle_start(&service))
                        }
                        ExternalEvent::Stop { service, notify } => {
                            notify.send(self.handle_stop(&service))
                        }
                        ExternalEvent::Restart { service, notify } => {
                            notify.send(self.handle_restart(&service))
                        }
                        ExternalEvent::Shutdown { notify } => self.handle_shutdown(notify),
                    },
                    SupervisorStatus::ShuttingDown { .. } => {
                        info!("rejecting because already shutting down");
                        external_event.reject(Error::ShuttingDown)
                    }
                    SupervisorStatus::Down => {
                        info!("rejecting because down");
                        // We are done now and have no more child processes to
                        // manage. It is possible that we external events were
                        // still in the queue, reject those.
                        external_event.reject(Error::ShuttingDown)
                    }
                },
            }
        }
    }

    fn handle_load_config(&mut self, config: config::Config) {
        let new_services: HashMap<String, Service> = config
            .0
            .iter()
            .map(|(service_name, service_config)| {
                (
                    service_name.to_owned(),
                    Service {
                        wanted: match self
                            .services
                            .get(service_name)
                            .map(|service| service.wanted)
                        {
                            Some(Wanted::Alive) => Wanted::Alive,
                            _ => match service_config.autostart {
                                true => Wanted::Alive,
                                false => Wanted::Dead,
                            },
                        },
                        name: service_name.to_owned(),
                        config: service_config.to_owned(),
                    },
                )
            })
            .collect();

        kill_orphans(self.event_queue.clone(), &mut self.processes, &new_services);
        for service in new_services.values() {
            kill_outdated(self.event_queue.clone(), &mut self.processes, &service);

            if service.did_change(&self.services) {
                apply_service(self.event_queue.clone(), &mut self.processes, &service);
            }
        }

        self.services = new_services;
    }

    fn handle_process_died(&mut self, pid: libc::pid_t, stat_loc: syscall::StatLoc) {
        // Our state should always reflect the world. If it ever doesn't we then
        // our code is broken and we should crash violently.
        let removed_process = self
            .processes
            .remove(&pid)
            .expect("process died that was not in processes list");

        let service_name = &removed_process.service;

        match stat_loc.exit_status() {
            Some(code) => info!(
                "Process {} from service {} died with exit code {}",
                pid, service_name, code
            ),
            None => info!(
                "Process {} from service {} died by signal",
                pid, service_name
            ),
        }

        if let SupervisorStatus::ShuttingDown { .. } = &self.status {
            if self.processes.len() == 0 {
                info!("All child processes died, shutting down");

                // We want to call notify.accept, but that moves notify. This
                // isn't an issue because we will replace the status with Down.
                // Rust will complain if we call accept directly though, as that
                // would move it out of self while we have a mutable reference
                // to it.
                //
                // To work around this, we swap self.status with the local
                // status, which both sets self.status to Down (preventing
                // future calls to notify), and gives us access to a notify that
                // we own and can therefore consume.
                let mut status = SupervisorStatus::Down;
                mem::swap(&mut self.status, &mut status);
                if let SupervisorStatus::ShuttingDown { notify } = status {
                    notify.accept(());
                }
            }
        }
        // If the process was not orphaned we might have to restart it.
        else if let Some(service) = self.services.get(service_name) {
            let should_restart = {
                // If kill_pending is true that means that it was us who tried
                // to kill the process. We don't want to automatically restart
                // process here as a replacement could have already started.
                !removed_process.kill_pending &&

                // Give up on restarting the process if we don't have any
                // retries left.
                removed_process.retries_left > 0 &&

                // We didn't kill the process ourselves, check if we should
                // restart it.
                match service.config.autorestart {
                    config::AutoRestart::Always => true,
                    config::AutoRestart::OnError => match stat_loc.exit_status() {
                        Some(code) => !service.config.exitcodes.contains(&code),
                        // We consider processes which died by signal to have
                        // errored.
                        None => true,
                    },
                    config::AutoRestart::Never => false,
                }
            };

            if should_restart {
                info!("Restarting process from service {}", service_name);
                spawn_process(
                    self.event_queue.clone(),
                    &mut self.processes,
                    service,
                    removed_process.retries_left - 1,
                );
            } else if removed_process.retries_left == 0 {
                info!(
                    "Reached startretries for service {}, not replacing process {}",
                    service_name, &pid
                );
            }
        }
    }

    fn handle_murder_process(&mut self, pid: libc::pid_t) {
        match self.processes.get(&pid) {
            None => {
                // Thankfully the process already exited by itself. No need to
                // do anything.
            }
            Some(process) => {
                info!("Murdering process {} from service {}", pid, process.service);
                if let Err(e) = syscall::kill(pid, libc::SIGKILL) {
                    if e.raw_os_error().expect("must always be an OS error") == libc::ESRCH {
                        // The process died already, but the event to process
                        // that has been queued behind us. Nothing to do.
                    } else {
                        error!("Failed to kill process {} with SIGKILL", pid);
                        error!("We will not attempt to kill this process again");
                    }
                }
            }
        }
    }

    fn handle_service_status(
        &self,
        service_name: Option<String>,
    ) -> Result<Vec<protocol::ServiceInfo>, Error> {
        if let Some(service_name) = &service_name {
            if !self.services.contains_key(service_name) {
                return Err(Error::UnknownService(service_name.clone()));
            }
        }

        let services_to_check = self
            .services
            .values()
            .filter(|service| match &service_name {
                Some(service_name) => &service.name == service_name,
                None => true,
            });

        let service_infos: Vec<protocol::ServiceInfo> = services_to_check
            .map(|service| protocol::ServiceInfo {
                name: service.name.clone(),
                status: service.status(&self.processes),
                processes: self
                    .processes
                    .iter()
                    .filter(|(_, process)| process.service == service.name)
                    .map(|(pid, process)| protocol::ProcessInfo {
                        pid: *pid,
                        status: process.status(&service.config),
                    })
                    .collect(),
            })
            .collect();

        Ok(service_infos)
    }

    fn handle_start(&mut self, service_name: &str) -> Result<(), Error> {
        match self.services.get_mut(service_name) {
            Some(service) => {
                info!("Starting service {}", service_name);
                service.wanted = Wanted::Alive;
                apply_service(self.event_queue.clone(), &mut self.processes, service);
                Ok(())
            }
            None => Err(Error::UnknownService(service_name.into())),
        }
    }

    fn handle_stop(&mut self, service_name: &str) -> Result<(), Error> {
        match self.services.get_mut(service_name) {
            Some(service) => {
                info!("Stopping service {}", service_name);
                service.wanted = Wanted::Dead;
                apply_service(self.event_queue.clone(), &mut self.processes, service);
                Ok(())
            }
            None => Err(Error::UnknownService(service_name.into())),
        }
    }

    fn handle_restart(&mut self, service_name: &str) -> Result<(), Error> {
        match self.services.get_mut(service_name) {
            Some(service) => {
                info!("Restarting service {}", service_name);
                service.wanted = Wanted::Dead;
                apply_service(self.event_queue.clone(), &mut self.processes, service);
                service.wanted = Wanted::Alive;
                apply_service(self.event_queue.clone(), &mut self.processes, service);
                Ok(())
            }
            None => Err(Error::UnknownService(service_name.into())),
        }
    }

    fn handle_shutdown(&mut self, notify: Notify<()>) {
        info!("Initiating shutdown");

        // If we don't have any child processes, we can exit right away.
        if self.processes.len() == 0 {
            info!("No child processes currently alive, shutting down");
            self.status = SupervisorStatus::Down;

            notify.accept(());
        } else {
            self.status = SupervisorStatus::ShuttingDown { notify };

            // Kill all our child processes. Once all child processes have died, we will exit.
            for (pid, process) in self.processes.iter_mut() {
                let service_config = self.services.get(&process.service).map(|s| &s.config);
                kill_process(self.event_queue.clone(), service_config, *pid, process);
            }
        }
    }
}

fn kill_orphans(
    event_queue: Sender<Event>,
    processes: &mut HashMap<libc::pid_t, Process>,
    services: &HashMap<String, Service>,
) {
    for (pid, process) in processes
        .iter_mut()
        .filter(|(_, process)| process.kill_pending == false)
    {
        if !services.contains_key(&process.service) {
            info!(
                "Killing orphaned process {} of non-existant service {}...",
                pid, process.service,
            );
            kill_process(event_queue.clone(), None, *pid, process);
        }
    }
}

fn kill_outdated(
    event_queue: Sender<Event>,
    processes: &mut HashMap<libc::pid_t, Process>,
    service: &Service,
) {
    let outdated_processes = processes.iter_mut().filter(|(_, process)| {
        process.service == service.name
            && !process.kill_pending
            && !process.matches_model(&service.config)
    });

    for (pid, process) in outdated_processes {
        info!(
            "Killing process {} of service {} because of outdated configuration...",
            pid, process.service,
        );
        kill_process(event_queue.clone(), Some(&service.config), *pid, process);
    }
}

fn apply_service(
    event_queue: Sender<Event>,
    processes: &mut HashMap<libc::pid_t, Process>,
    service: &Service,
) {
    let amount_wanted_alive = match service.wanted {
        Wanted::Alive => service.config.replicas,
        Wanted::Dead => 0,
    } as usize;

    let amount_alive = processes
        .iter()
        .filter(|(_, process)| process.service == service.name && !process.kill_pending)
        .count();

    if amount_alive < amount_wanted_alive {
        // We have too little process left, spin up some more.
        info!(
            "Starting {} processes for {}...",
            amount_wanted_alive - amount_alive,
            service.name
        );
        for _ in amount_alive..amount_wanted_alive {
            spawn_process(
                event_queue.clone(),
                processes,
                &service,
                service.config.startretries,
            );
        }
    } else if amount_alive > amount_wanted_alive {
        // We have too many processes, kill a few.
        info!(
            "Killing {} processes for {}...",
            amount_alive - amount_wanted_alive,
            service.name,
        );
        for _ in amount_wanted_alive..amount_alive {
            let (pid, process) = processes
                .iter_mut()
                .find(|(_, process)| {
                    process.service == service.name && process.kill_pending == false
                })
                .expect("must always find process if amount_alive > 0");

            kill_process(event_queue.clone(), Some(&service.config), *pid, process);
        }
    } else {
        // Nothing to do!
    }
}

fn spawn_process(
    event_queue: Sender<Event>,
    processes: &mut HashMap<libc::pid_t, Process>,
    service: &Service,
    retries_left: u32,
) {
    let mut argv: Vec<&CStr> = service.config.args.iter().map(|s| s.as_ref()).collect();
    argv.insert(0, service.config.command.as_ref());

    let pid = match syscall::spawn(
        &argv,
        service.config.umask,
        service.config.workingdir.as_ref().map(|s| s.as_ref()),
        service.config.stdout.as_ref(),
        service.config.stderr.as_ref(),
        &service.config.env,
    ) {
        Ok(pid) => pid,
        Err(e) => {
            warn!(
                "Failed to spawn process for service {}: {}",
                service.name, e,
            );
            return;
        }
    };

    processes.insert(
        pid,
        Process {
            command: service.config.command.clone(),
            args: service.config.args.clone(),
            umask: service.config.umask.clone(),
            workingdir: service.config.workingdir.clone(),
            stdout: service.config.stdout.clone(),
            stderr: service.config.stderr.clone(),
            env: service.config.env.clone(),
            service: service.name.clone(),
            start_moment: time::Instant::now(),
            kill_pending: false,
            retries_left,
        },
    );

    monitor_child(event_queue, pid);
}

fn kill_process(
    event_queue: Sender<Event>,
    service_config: Option<&config::Service>,
    pid: libc::pid_t,
    process: &mut Process,
) {
    process.kill_pending = true;

    let (kill_signal, check_if_dead_delay) = match service_config {
        Some(config) => (
            config.stopsignal as libc::c_int,
            time::Duration::from_millis(config.stoptime_ms),
        ),
        None => (libc::SIGTERM, time::Duration::from_millis(10_000)),
    };

    if let Err(e) = syscall::kill(pid, kill_signal) {
        if e.raw_os_error().expect("must always be an OS error") == libc::ESRCH {
            // The process died already, but the event to process
            // that has been queued behind us. Nothing to do.
        } else {
            warn!(
                "Failed to tell process {} from service {} to exit: {}",
                pid, process.service, e
            );
            warn!(
                "Retrying with SIGKILL in {} ms...",
                check_if_dead_delay.as_millis()
            );
        }
    }

    send_event_delayed(
        event_queue,
        Event::Internal(InternalEvent::MurderProcess { pid }),
        check_if_dead_delay,
    );
}

fn monitor_child(event_queue: Sender<Event>, pid: libc::pid_t) {
    thread::spawn(move || {
        let stat_loc = syscall::wait(pid).expect("wait must not fail when called with valid pid");
        event_queue.send(Event::Internal(InternalEvent::ProcessDied {
            pid,
            stat_loc,
        }))
    });
}

fn send_event_delayed(event_queue: Sender<Event>, event: Event, delay: time::Duration) {
    thread::spawn(move || {
        thread::sleep(delay);
        event_queue
            .send(event)
            .expect("supervisor must always be alive")
    });
}

#[derive(Clone)]
pub struct Bridge(Sender<Event>);

impl Bridge {
    pub fn load_config(&self, config: config::Config) -> Result<(), Error> {
        let (tx, rx) = channel::<Result<(), Error>>();
        self.0
            .send(Event::External(ExternalEvent::LoadConfig {
                config,
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }

    pub fn status(&self, service: Option<String>) -> Result<Vec<protocol::ServiceInfo>, Error> {
        let (tx, rx) = channel::<Result<Vec<protocol::ServiceInfo>, Error>>();
        self.0
            .send(Event::External(ExternalEvent::ServiceStatus {
                service: service,
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }

    pub fn start(&self, service: &str) -> Result<(), Error> {
        let (tx, rx) = channel::<Result<(), Error>>();
        self.0
            .send(Event::External(ExternalEvent::Start {
                service: service.to_owned(),
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }

    pub fn stop(&self, service: &str) -> Result<(), Error> {
        let (tx, rx) = channel::<Result<(), Error>>();
        self.0
            .send(Event::External(ExternalEvent::Stop {
                service: service.to_owned(),
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }

    pub fn restart(&self, service: &str) -> Result<(), Error> {
        let (tx, rx) = channel::<Result<(), Error>>();
        self.0
            .send(Event::External(ExternalEvent::Restart {
                service: service.to_owned(),
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }

    pub fn shutdown(&self) -> Result<(), Error> {
        let (tx, rx) = channel::<Result<(), Error>>();
        self.0
            .send(Event::External(ExternalEvent::Shutdown {
                notify: Notify(tx),
            }))
            .expect("supervisor must always be alive");
        rx.recv().expect("channel closed before response was sent")
    }
}

#[derive(Debug)]
pub enum Error {
    UnknownService(String),
    ShuttingDown,
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UnknownService(name) => write!(f, "Unknown service {}", name),
            Error::ShuttingDown => write!(f, "Supervisor is shutting down"),
        }
    }
}

impl From<Error> for protocol::Response {
    fn from(error: Error) -> Self {
        match error {
            Error::UnknownService(name) => protocol::Response::UnknownService { service: name },
            Error::ShuttingDown => protocol::Response::ShuttingDown,
        }
    }
}
