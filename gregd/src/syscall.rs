use std::collections::HashMap;
use std::env;
use std::ffi::{CStr, CString, OsStr};
use std::fs::File;
use std::io::{self, Read, Write};
use std::os::unix::prelude::{FromRawFd, OsStrExt};
use std::ptr;

use crate::config;

pub fn kill(pid: libc::pid_t, sig: libc::c_int) -> io::Result<()> {
    let ret = unsafe { libc::kill(pid, sig) };
    if ret == -1 {
        return Err(io::Error::last_os_error());
    }

    Ok(())
}

fn pipe_cloexec() -> io::Result<(File, File)> {
    let mut fds = [0; 2];

    unsafe {
        // We cannot use libc::pipe2 as it is not available on Darwin.
        if libc::pipe(fds.as_mut_ptr()) == -1 {
            return Err(io::Error::last_os_error());
        }
        if libc::fcntl(fds[1], libc::F_SETFD, libc::FD_CLOEXEC) == -1 {
            return Err(io::Error::last_os_error());
        }

        Ok((File::from_raw_fd(fds[0]), File::from_raw_fd(fds[1])))
    }
}

fn to_c_argv(argv: &[&CStr]) -> Vec<*const libc::c_char> {
    let mut argv: Vec<*const libc::c_char> = argv.iter().map(|cstring| cstring.as_ptr()).collect();
    argv.push(ptr::null());

    return argv;
}

// Inspired by std::sys::unix::cvt
fn c_func_ok(ret: libc::c_int) -> io::Result<libc::c_int> {
    if ret == -1 {
        Err(io::Error::last_os_error())
    } else {
        Ok(ret)
    }
}

unsafe fn redirect_out_file(path: &CStr, dest_fd: libc::c_int) -> io::Result<libc::c_int> {
    let fd = c_func_ok(libc::open(
        path.as_ptr(),
        // Create the file to redirect output to if it doesn't exist,
        // open it for writing in append mode, and set the CLOEXEC flag so the original fd will be closed once we exec.
        // dup2 does not transfer the CLOEXEC flag, so the new fd replacing dest_fd will not be closed.
        libc::O_WRONLY | libc::O_CREAT | libc::O_APPEND | libc::O_CLOEXEC,
        // If the file needs to be created, create it with rw-r--r-- file permissions.
        (libc::S_IRUSR | libc::S_IWUSR | libc::S_IRGRP | libc::S_IROTH) as libc::c_uint,
    ))?;
    c_func_ok(libc::dup2(fd, dest_fd))
}

unsafe fn do_exec(
    argv: Vec<*const libc::c_char>,
    umask: Option<libc::mode_t>,
    cwd: Option<&CStr>,
    stdout_path: &CStr,
    stderr_path: &CStr,
    env: &HashMap<config::EnvKey, CString>,
) -> Result<(), io::Error> {
    // Do the output redirections BEFORE changing the workingdir or umask,
    // to avoid the workingdir and umask affecting the files created for redirection.
    redirect_out_file(stdout_path, libc::STDOUT_FILENO)?;
    redirect_out_file(stderr_path, libc::STDERR_FILENO)?;

    if let Some(dir) = cwd {
        c_func_ok(libc::chdir(dir.as_ptr()))?;
        env::set_var("PWD", OsStr::from_bytes(dir.to_bytes()));
    }
    if let Some(mask) = umask {
        libc::umask(mask); // umask cannot fail
    }

    for (key, value) in env {
        // set_var would panic if key is empty or contains '=' or '\0', or if value contains '\0',
        // but by using config::EnvKey we are able to assert none of those cases are possible during deserialization.
        env::set_var(
            OsStr::from_bytes(key.0.to_bytes()),
            OsStr::from_bytes(value.to_bytes()),
        );
    }

    libc::execvp(argv[0], argv.as_ptr());
    // If we get past execvp, it failed, so we just return the last errno.
    Err(io::Error::last_os_error())
}

pub fn spawn(
    argv: &[&CStr],
    umask: Option<libc::mode_t>,
    cwd: Option<&CStr>,
    stdout_path: &CStr,
    stderr_path: &CStr,
    env: &HashMap<config::EnvKey, CString>,
) -> io::Result<libc::pid_t> {
    let argv = to_c_argv(argv);

    // Create a new set of file descriptors for communicating between the
    // parent and child process. This is solely used for communicating
    // exec errors with the parent process. We set CLOEXEC to automatically
    // close the fd upon exit so that we can communicate that exec was
    // successful.
    let (mut input, mut output) = pipe_cloexec()?;

    unsafe {
        let pid = libc::fork();
        if pid == 0 {
            // We are the child process
            drop(input);

            let errno = do_exec(argv, umask, cwd, stdout_path, stderr_path, env)
                .unwrap_err()
                .raw_os_error()
                .expect("must always be an OS error");
            output
                .write(&errno.to_be_bytes())
                .expect("failed to tell gregd about error");

            // We run _exit instead of exit to ensure no side-effects happen
            // when exiting.
            libc::_exit(127);
        } else {
            // We are parent process
            if pid == -1 {
                // Forking failed
                return Err(io::Error::last_os_error());
            }

            drop(output);
            let mut bytes = [0; 4];
            loop {
                match input.read(&mut bytes) {
                    Ok(0) => {
                        return Ok(pid);
                    }
                    Ok(4) => return Err(io::Error::from_raw_os_error(i32::from_be_bytes(bytes))),
                    Err(ref e) if e.kind() == io::ErrorKind::Interrupted {} => {}
                    Ok(..) => panic!("writes should be atomic"),
                    Err(_) => panic!("failed to read from pipe"),
                }
            }
        }
    }
}

pub fn wait(pid: libc::pid_t) -> io::Result<StatLoc> {
    let mut status = 0 as libc::c_int;
    let ret = unsafe { libc::waitpid(pid, &mut status, 0) };
    if ret == -1 {
        return Err(io::Error::last_os_error());
    }

    Ok(StatLoc(status))
}

// Inspired by std::sys::unix::process::ExitStatus.
pub struct StatLoc(libc::c_int);

impl StatLoc {
    pub fn exited(&self) -> bool {
        libc::WIFEXITED(self.0)
    }

    pub fn exit_status(&self) -> Option<i8> {
        self.exited().then(|| libc::WEXITSTATUS(self.0) as i8)
    }
}
