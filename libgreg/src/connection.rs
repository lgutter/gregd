use std::env;
use std::error;
use std::fmt;
use std::fs;
use std::io::BufRead;
use std::io::Write;
use std::io::{self, BufReader};
use std::os::unix::net::UnixListener;
use std::os::unix::net::UnixStream;
use std::path::Path;
use std::path::PathBuf;

pub fn path() -> PathBuf {
    let path_string = match env::var("XDG_RUNTIME_DIR") {
        Ok(dir) => format!("{}/gregd.sock", dir),
        Err(_) => match env::var("USER") {
            Ok(user) => format!("/tmp/gregd-{}.sock", user),
            Err(_) => String::from("/tmp/gregd.sock"),
        },
    };

    PathBuf::from(path_string)
}

#[derive(Debug)]
pub enum Error {
    WriteError(io::Error),
    ReadError(io::Error),
    SocketAlreadyBound(PathBuf),
    SocketBindError(PathBuf, io::Error),
    SocketConnectError(PathBuf, io::Error),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::WriteError(ref e) => write!(f, "Failed to write to socket: {}", e),
            Error::ReadError(ref e) => write!(f, "Failed to read from socket: {}", e),
            Error::SocketAlreadyBound(path) => {
                write!(f, "Already bound: {}", path.display())
            }
            Error::SocketBindError(path, ref e) => {
                write!(f, "Failed to bind to {}: {}", path.display(), e)
            }
            Error::SocketConnectError(path, ref e) => {
                write!(f, "Failed to connect to {}: {}", path.display(), e)
            }
        }
    }
}

pub struct SocketLink {
    path: PathBuf,
    did_bind: bool,
}

impl SocketLink {
    pub fn new(path: &Path) -> SocketLink {
        SocketLink {
            path: path.into(),
            did_bind: false,
        }
    }

    pub fn bind(&mut self) -> Result<UnixListener, Error> {
        if self.did_bind {
            return Err(Error::SocketAlreadyBound(self.path.clone()));
        }

        match UnixListener::bind(&self.path) {
            Ok(unix_listener) => {
                self.did_bind = true;
                Ok(unix_listener)
            }
            Err(e) => Err(Error::SocketBindError(self.path.clone(), e)),
        }
    }

    pub fn connect(&mut self) -> Result<UnixStream, Error> {
        match UnixStream::connect(&self.path) {
            Err(e) => Err(Error::SocketConnectError(self.path.clone(), e)),
            Ok(unix_stream) => Ok(unix_stream),
        }
    }
}

impl Drop for SocketLink {
    fn drop(&mut self) {
        if self.did_bind {
            if let Err(e) = fs::remove_file(&self.path) {
                eprintln!("Failed to remove {}: {}", self.path.display(), e)
            }
        }
    }
}

// The UnixStreamReader and UnixStreamWriter structs have been created as to
// allow spitting the io::Read and io::Write traits of UnixStream into separate
// structs. Splitting them like this will allow moving the reading part to the
// more advanced BufReader, without losing access to the writing part.
struct UnixStreamReader<'a>(&'a UnixStream);

impl io::Read for UnixStreamReader<'_> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.0.read(buf)
    }
}

struct UnixStreamWriter<'a>(&'a UnixStream);

impl io::Write for UnixStreamWriter<'_> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.0.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.0.flush()
    }
}

/// `Connection` allows for the reading and writing of serialized messages on
/// either end of a `UnixStream`.
///
/// # Examples
///
/// ```
/// use std::os::unix::net::UnixStream;
/// use libgreg::connection::Connection;
/// use libgreg::protocol::{Request, Message, DELIM};
///
/// # use std::error::Error;
/// # fn main() -> Result<(), Box<dyn Error>> {
/// let (sock1, sock2) = UnixStream::pair()?;
///
/// let mut connection1 = Connection::new(&sock1);
/// let mut connection2 = Connection::new(&sock2);
///
/// let request = Request::LogMessage { message: String::from("test") };
/// let request_serialized = request.serialize();
/// connection1.write_message(&request_serialized)?;
///
/// let read_serialized = connection2.read_message(DELIM)?;
/// let read = Request::deserialize(&read_serialized)?;
///
/// assert_eq!(read, request);
/// #     Ok(())
/// # }
/// ```
pub struct Connection<'a> {
    reader: BufReader<UnixStreamReader<'a>>,
    writer: UnixStreamWriter<'a>,
}

impl<'a> Connection<'a> {
    pub fn new(unix_stream: &'a UnixStream) -> Connection<'a> {
        Connection {
            reader: BufReader::new(UnixStreamReader(unix_stream)),
            writer: UnixStreamWriter(unix_stream),
        }
    }

    pub fn read_message(&mut self, delim: u8) -> Result<Vec<u8>, Error> {
        let mut buf: Vec<u8> = Vec::new();

        match self.reader.read_until(delim, &mut buf) {
            Err(e) => Err(Error::ReadError(e)),
            Ok(_) => Ok(buf),
        }
    }

    pub fn write_message(&mut self, buf: &[u8]) -> Result<(), Error> {
        if let Err(e) = self.writer.write_all(buf) {
            return Err(Error::WriteError(e));
        }
        self.writer.flush().map_err(Error::WriteError)
    }
}
