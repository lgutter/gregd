use serde::{Deserialize, Serialize};
use std::error;
use std::fmt;

pub const DELIM: u8 = 0b0;

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum Request {
    /// No-op, useful for testing if the connection is alive.
    ///
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Ping;
    /// let serialized = Vec::from("{\"type\":\"ping\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Ping,

    /// Requests a message to be logged by the daemon.
    ///
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::LogMessage { message: "Test für gregd".into() };
    /// let serialized = Vec::from("{\"type\":\"log_message\",\"message\":\"Test für gregd\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    LogMessage { message: String },

    /// Reload the configuration from the predefined config file.
    ///
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Reload;
    /// let serialized = Vec::from("{\"type\":\"reload\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Reload,

    // Start the service as specified in the config. If we gave up on restarting
    // any process, calling this on an already started service will cause us to
    // try again.
    //
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Start { service: "foo".into() };
    /// let serialized = Vec::from("{\"type\":\"start\",\"service\":\"foo\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Start { service: String },

    // Stop a service, killing all processes of that service. If autostart is
    // set to true the service will be started again when reloading the config.
    //
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Stop { service: "foo".into() };
    /// let serialized = Vec::from("{\"type\":\"stop\",\"service\":\"foo\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Stop { service: String },

    // Restart a service. This is equivalent to first sending the stop command
    // and then sending the start command. Note that the new processes are
    // started immediately and do not wait on the old processes to die.
    //
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Restart { service: "foo".into() };
    /// let serialized = Vec::from("{\"type\":\"restart\",\"service\":\"foo\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Restart { service: String },

    /// Request the status of the given service.
    ///
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Status { service: Some("ls".into()) };
    /// let serialized = Vec::from("{\"type\":\"status\",\"service\":\"ls\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Status { service: Option<String> },

    /// Tell gregd to gracefully shut down, killing all child processes
    /// and exiting once all child processes have died.
    ///
    /// ```
    /// use libgreg::protocol::{Request, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Request::Shutdown;
    /// let serialized = Vec::from("{\"type\":\"shutdown\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Request::deserialize(&serialized)?, deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Shutdown,
}

impl Message<'_> for Request {}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
#[serde(tag = "type", rename_all = "snake_case")]
pub enum Response {
    /// Generic success response, for when no additional data needed to be
    /// provided.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::Ok;
    /// let serialized = Vec::from("{\"type\":\"ok\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Ok,

    /// Only sent in case we did not understand the client's request.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::BadRequest;
    /// let serialized = Vec::from("{\"type\":\"bad_request\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    BadRequest,

    /// The request failed because the config could not be loaded.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::ConfigError{ message: String::from("aaaaaaaaaaaahhh") };
    /// let serialized = Vec::from("{\"type\":\"config_error\",\"message\":\"aaaaaaaaaaaahhh\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    ConfigError { message: String },

    /// The request failed because the given service did not exist.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::UnknownService { service: "foo".into() };
    /// let serialized = Vec::from("{\"type\":\"unknown_service\",\"service\":\"foo\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    UnknownService { service: String },

    /// The request failed because the config could not be loaded.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::Status { services: Vec::new() };
    /// let serialized = Vec::from("{\"type\":\"status\",\"services\":[]}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    Status { services: Vec<ServiceInfo> },

    /// The request could not be completed because we are currently shutting
    /// down.
    ///
    /// ```
    /// use libgreg::protocol::{Response, Message};
    ///
    /// # use std::error::Error;
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// let deserialized = Response::ShuttingDown;
    /// let serialized = Vec::from("{\"type\":\"shutting_down\"}\0");
    ///
    /// assert_eq!(deserialized.serialize(), serialized);
    /// assert_eq!(Response::deserialize(&serialized).unwrap(), deserialized);
    /// #     Ok(())
    /// # }
    /// ```
    ShuttingDown,
}

impl Message<'_> for Response {}

pub trait Message<'a>: Serialize + Deserialize<'a> {
    /// Prettyprints the Message in a human-readable format, useful for
    /// debugging.
    fn pretty(&self) -> String {
        serde_json::to_string_pretty(self).expect("must serialize")
    }

    /// Turns the Message into a serialized format in compliant with the
    /// protocol. Ready to be send over the socket.
    fn serialize(&self) -> Vec<u8> {
        let mut serialized = serde_json::to_vec(self).expect("must serialize");
        serialized.push(0b0);
        serialized
    }

    /// Converts a protocol-compliant message into a rust struct.
    fn deserialize(serialized: &'a [u8]) -> Result<Self, Error> {
        match serialized.split_last() {
            Some((&DELIM, serialized_json)) => match serde_json::from_slice(serialized_json) {
                Ok(request) => Ok(request),
                Err(e) => Err(Error::InvalidJSON(e)),
            },
            Some((_, _)) => Err(Error::InvalidDelim),
            None => Err(Error::EmptyMessage),
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub enum ProcessStatus {
    Starting,
    Running,
    Terminating,
}

impl fmt::Display for ProcessStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ProcessStatus::Starting => f.write_str("starting"),
            ProcessStatus::Running => f.write_str("running"),
            ProcessStatus::Terminating => f.write_str("terminating"),
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ProcessInfo {
    pub pid: libc::pid_t,
    pub status: ProcessStatus,
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub enum ServiceStatus {
    Active,
    Inactive,
    Degraded,
}

impl fmt::Display for ServiceStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ServiceStatus::Active => f.write_str("active"),
            ServiceStatus::Inactive => f.write_str("inactive"),
            ServiceStatus::Degraded => f.write_str("degraded"),
        }
    }
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct ServiceInfo {
    pub name: String,
    pub status: ServiceStatus,
    pub processes: Vec<ProcessInfo>,
}

#[derive(Debug)]
pub enum Error {
    EmptyMessage,
    InvalidDelim,
    InvalidJSON(serde_json::Error),
}

impl error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::EmptyMessage => write!(f, "Message contains no data"),
            Error::InvalidDelim => write!(f, "Doesn't end with null-byte"),
            Error::InvalidJSON(ref e) => write!(f, "Invalid JSON: {}", e),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Error, Message, Request};

    #[test]
    fn pretty_prints_pretty() {
        let request = Request::deserialize(&Vec::from("{\"type\":\"ping\"}\0")).unwrap();

        assert_eq!(request.pretty(), "{\n  \"type\": \"ping\"\n}")
    }

    #[test]
    fn deserialize_errors_on_invalid_json() {
        match Request::deserialize(&Vec::from("{\0")) {
            Err(Error::InvalidJSON(_)) => return,
            Ok(_) => panic!("should error"),
            Err(_) => panic!("should error differently"),
        }
    }

    #[test]
    fn deserialize_errors_on_missing_field() {
        match Request::deserialize(&Vec::from("{\"type\":\"log_message\"}\0")) {
            Err(Error::InvalidJSON(_)) => return,
            Ok(_) => panic!("should error"),
            Err(_) => panic!("should error differently"),
        }
    }

    #[test]
    fn deserialize_errors_on_missing_delimiter() {
        match Request::deserialize(&Vec::from("{\"type\":\"ping\"}")) {
            Err(Error::InvalidDelim) => return,
            Ok(_) => panic!("should error"),
            Err(_) => panic!("should error differently"),
        }
    }

    #[test]
    fn deserialize_errors_on_empty_input() {
        match Request::deserialize(&Vec::new()) {
            Err(Error::EmptyMessage) => return,
            Ok(_) => panic!("should error"),
            Err(_) => panic!("should error differently"),
        }
    }
}
