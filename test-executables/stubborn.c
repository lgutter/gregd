#include <unistd.h>
#include <signal.h>

void do_nothing(int stubborn) {
	(void)stubborn;
	// This space is intentionally left blank.
}

int main(void) {
	signal(SIGINT, &do_nothing);
	signal(SIGTERM, &do_nothing);

	while (1) {
		sleep(1000);
	}
}
